import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { CategoriasComponent } from './pages/categorias/categorias.component';
import { CategoriasEdicionComponent } from './pages/categorias/categorias-edicion/categorias-edicion.component';
import { VacantesComponent } from './pages/vacantes/vacantes.component';
import { VacantesEdicionComponent } from './pages/vacantes/vacantes-edicion/vacantes-edicion.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'pages/inicio'
  },
  {path: 'pages/inicio', component: InicioComponent},
  {path: 'pages/categorias', component: CategoriasComponent, children: [
    { path: 'nuevo', component: CategoriasEdicionComponent },
    { path: 'edicion/:id', component: CategoriasEdicionComponent }
  ]},
  {path: 'pages/vacantes', component: VacantesComponent, children: [
    { path: 'nuevo', component: VacantesEdicionComponent },
    { path: 'edicion/:id', component: VacantesEdicionComponent }
  ]}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
