import { Categoria } from './categoria';

export class Vacante {
    idVacante: number;
    nombre: string;
    descripcion: string;
    fecha: string;
    salario: number;
    destacado: number;
    imagen:string;
    detalles:string;
    estado:boolean;
    categoria: Categoria
}