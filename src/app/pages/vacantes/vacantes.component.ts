import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Vacante } from '../../_model/vacante';
import { VacanteService } from '../../_service/vacante.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-vacantes',
  templateUrl: './vacantes.component.html',
  styleUrls: ['./vacantes.component.css']
})
export class VacantesComponent implements OnInit {
  
  dataSource: MatTableDataSource<Vacante>;
  displayedColumns: string[] = ['idVacante', 'nombre', 'descripcion', 'fecha','acciones'];

  constructor(
    private vacanteService: VacanteService,
    public route: ActivatedRoute,
    public snackBar: MatSnackBar

  ) { }

  ngOnInit(): void {
    this.vacanteService.listar().subscribe(data => {
      this.crearTabla(data);
    });

    this.vacanteService.getVacanteCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.vacanteService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });

  }

  crearTabla(data: Vacante[]) {
    this.dataSource = new MatTableDataSource(data);
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  eliminar(id: number) {
    this.vacanteService.eliminar(id).subscribe(() => {
      this.vacanteService.listar().subscribe(data => {
        this.vacanteService.setVacanteCambio(data);
        this.vacanteService.setMensajeCambio('El registro se eliminó...');
      });
    });
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

}
