import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Vacante } from '../../../_model/vacante';
import { VacanteService } from '../../../_service/vacante.service';
import { Categoria } from '../../../_model/categoria';

//import * as moment from 'moment';
import { CategoriaService } from '../../../_service/categoria.service';

@Component({
  selector: 'app-vacantes-edicion',
  templateUrl: './vacantes-edicion.component.html',
  styleUrls: ['./vacantes-edicion.component.css']
})
export class VacantesEdicionComponent implements OnInit {

  vacante: Vacante=<Vacante>{};
  form: FormGroup=<FormGroup>{};
  idVacante: number;
  edicion:boolean;
  categorias: Categoria[];
  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();

  idCategoriaSeleccionada: number;

  constructor(
    private vacanteServicio: VacanteService,
    private categoriasService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'nombre': new FormControl(''),
      'descripcion': new FormControl(''),
      'fecha': new FormControl(''),
      'salario': new FormControl(''),
      'destacado': new FormControl(''),
      'imagen': new FormControl(''),
      'detalles': new FormControl(''),
      'estado': new FormControl(''),
      'categoria': new FormControl('')
    });
    this.listarCategorias();
    this.route.params.subscribe((data: Params) => {
      this.idVacante = data['id'];
      console.log(this.idVacante);
      this.edicion = data['id'] != null;
      if (this.edicion){
        this.vacanteServicio.buscarPorId(this.idVacante)
                  .subscribe(async data=>{  this.vacante= await data;
                   this.idCategoriaSeleccionada = await this.vacante.categoria.idCategoria;
                   //this.myControlCategorias.setValue(this.categoriaSeleccionada);
                   this.fechaSeleccionada = new Date(this.vacante.fecha); 
                  })
      }
    });
  }

  listarCategorias() {
    this.categoriasService.listar().subscribe(data => {
      this.categorias = data;
    });
  }

  grabar() {

    if (this.edicion) {
      //MODIFICAR
      this.vacanteServicio.modificar(this.vacante).subscribe( ()=> {
        this.vacanteServicio.listar().subscribe(data => {
          this.vacanteServicio.setVacanteCambio(data);
          this.vacanteServicio.setMensajeCambio('Los datos se modificaron correctamente.');
        })
      })
    } else {
      //REGISTRAR
      this.vacanteServicio.registrar(this.vacante).subscribe(() => {
        this.vacanteServicio.listar().subscribe(data => {
          this.vacanteServicio.setVacanteCambio(data);
          this.vacanteServicio.setMensajeCambio('Los datos se registraron correctamente.');
        })
      });
  
     }
    this.router.navigate(['/pages/vacantes']); 
  }

  cancelar() {
    this.limpiarControles();
    this.router.navigate(['/pages/vacantes']); 
  }

  limpiarControles() {

    this.vacante = null;
  

  }



}
