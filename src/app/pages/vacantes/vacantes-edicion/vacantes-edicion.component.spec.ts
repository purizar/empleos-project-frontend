import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VacantesEdicionComponent } from './vacantes-edicion.component';

describe('VacantesEdicionComponent', () => {
  let component: VacantesEdicionComponent;
  let fixture: ComponentFixture<VacantesEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VacantesEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VacantesEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
