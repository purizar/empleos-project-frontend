import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../../_service/categoria.service';
import { Categoria } from '../../_model/categoria';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  dataSource: MatTableDataSource<Categoria>;
  displayedColumns: string[] = ['idCategoria', 'nombre', 'descripcion', 'acciones'];

  constructor(private categoriaService: CategoriaService,
              public route: ActivatedRoute,
              public snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.categoriaService.listar().subscribe(data => {
      this.crearTabla(data);
    });

    this.categoriaService.getCategoriaCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.categoriaService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "right"
      });
    });

  }

  crearTabla(data: Categoria[]) {
    this.dataSource = new MatTableDataSource(data);
    //this.dataSource.paginator = this.paginator;
    //this.dataSource.sort = this.sort;
  }

  eliminar(id: number) {
    this.categoriaService.eliminar(id).subscribe(() => {
      this.categoriaService.listar().subscribe(data => {
        this.categoriaService.setCategoriaCambio(data);
        this.categoriaService.setMensajeCambio('El registro se eliminó...');
      });
    });
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }





}
