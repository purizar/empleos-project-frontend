import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoriasEdicionComponent } from './categorias-edicion.component';

describe('CategoriasEdicionComponent', () => {
  let component: CategoriasEdicionComponent;
  let fixture: ComponentFixture<CategoriasEdicionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoriasEdicionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoriasEdicionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
