import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Categoria } from '../../../_model/categoria';
import { CategoriaService } from '../../../_service/categoria.service';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-categorias-edicion',
  templateUrl: './categorias-edicion.component.html',
  styleUrls: ['./categorias-edicion.component.css']
})
export class CategoriasEdicionComponent implements OnInit {

  categoria: Categoria=<Categoria>{};
  form: FormGroup;
  idCategoria: number;
  edicion:boolean;

  constructor(
    private catService: CategoriaService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'nombre': new FormControl(''),
      'descripcion': new FormControl('')
    });
    this.route.params.subscribe((data: Params) => {
      this.idCategoria = data['id'];
      console.log(this.idCategoria);
      this.edicion = data['id'] != null;
      if (this.edicion){
        this.catService.buscarPorId(this.idCategoria)
                  .subscribe(async data=>{  this.categoria= await data});
      }
    });
  }

  grabar() {

    if (this.edicion) {
      //MODIFICAR
      this.catService.modificar(this.categoria).subscribe( ()=> {
        this.catService.listar().subscribe(data => {
          this.catService.setCategoriaCambio(data);
          this.catService.setMensajeCambio('Los datos se modificaron correctamente.');
        })
      })
    } else {
      //REGISTRAR
      this.catService.registrar(this.categoria).subscribe(() => {
        this.catService.listar().subscribe(data => {
          this.catService.setCategoriaCambio(data);
          this.catService.setMensajeCambio('Los datos se registraron correctamente.');
        })
      });
  
     }
    this.router.navigate(['/pages/categorias']); 
  }

  cancelar() {
    this.limpiarControles();
    this.router.navigate(['/pages/categorias']); 
  }

  limpiarControles() {

    this.categoria = null;
  

  }


}
