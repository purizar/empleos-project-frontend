import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Vacante } from '../_model/vacante';

@Injectable({
  providedIn: 'root'
})
export class VacanteService {

  private vacanteCambio: Subject<Vacante[]> = new Subject<Vacante[]>();
  private mensajeCambio: Subject<string> = new Subject<string>();

  constructor(protected http: HttpClient) { }

  listar() {
    return this.http.get<Vacante[]>(environment.HOST + "/vacantes");

  }

  buscarPorId(id: number) {
    return this.http.get<Vacante>(environment.HOST + "/vacantes/"+id);
  }

  registrar(cat: Vacante) {
    return this.http.post(environment.HOST + "/vacantes", cat);
  }

  modificar(cat: Vacante) {
    return this.http.put(environment.HOST + "/vacantes", cat);
  }

  eliminar(id: number) {
    return this.http.delete(environment.HOST + "/vacantes/"+id);
  }

  getVacanteCambio() {
    return this.vacanteCambio.asObservable();
  }

  setVacanteCambio(lista: Vacante[]) {
    this.vacanteCambio.next(lista);
  }

  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }
}
