import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
import { Categoria } from '../_model/categoria';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private categoriaCambio: Subject<Categoria[]> = new Subject<Categoria[]>();
  private mensajeCambio: Subject<string> = new Subject<string>();

  constructor(protected http: HttpClient) { }

  listar() {
    return this.http.get<Categoria[]>(environment.HOST + "/categorias");

  }

  buscarPorId(id: number) {
    return this.http.get<Categoria>(environment.HOST + "/categorias/"+id);
  }

  registrar(cat: Categoria) {
    return this.http.post(environment.HOST + "/categorias", cat);
  }

  modificar(cat: Categoria) {
    return this.http.put(environment.HOST + "/categorias", cat);
  }

  eliminar(id: number) {
    return this.http.delete(environment.HOST + "/categorias/"+id);
  }

  getCategoriaCambio() {
    return this.categoriaCambio.asObservable();
  }

  setCategoriaCambio(lista: Categoria[]) {
    this.categoriaCambio.next(lista);
  }

  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }
}
